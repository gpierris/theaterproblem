#author: Georgios Pierris
#website: www.pierris.gr

#run for a 20x20 theater with group of people 2:   
# python theaterProblem.py 20 20 2

import random
import sys
import numpy as np
import math

import pylab

class Theater:

    def __init__(self, length, width, sizeOfGroup=2):
        self.length = length
        self.width = width
        self.totalSeats = self.length*self.width
        self.sizeOfGroup = sizeOfGroup

        self.seatsPlan = np.zeros((self.length, self.width)) # 0 is vacant, 1 will be occupied
        self.vacantSeats = []



    #Very very very very sloooowwww.... It will be faster if we go one row at a time and update every self.length times
    #Smart things can happen in the future. It is too late now and I am eager to get the results
    def findEmptyBlocksForGroup(self): #Start randomly and as you fail be more clever on allocating seats

        self.vacantSeats = []

        for row in range(self.length): #We start
            for col in range(self.width-self.sizeOfGroup+1): #Note for -sizeOfGroup: Ignore when we are at the top right and we know we can't fit them
                
                vacantBlock = False
                
                for person in range(self.sizeOfGroup):
                    if( self.seatsPlan[row][col+person] == 0 ):
                        vacantBlock = True #I overwrite it multiple times. If one is found occupied then it will be false and I break
                    else:
                        vacantBlock = False
                        break

                if(vacantBlock):
                    self.vacantSeats.append((row, col)) # row, col will be the starting seat and we will occupy another self.sizeOfGroup seats on the right


    def getRandomBlockToOccupy(self):

        return self.vacantSeats[ random.randint(0, len(self.vacantSeats)-1) ]

    def occupyRandomEmptyBlock(self):

        if(len(self.vacantSeats) == 0):
            return


        row, col = self.getRandomBlockToOccupy()

        for person in range(self.sizeOfGroup):
            self.seatsPlan[row][col+person] = 1.0


    def countOccupiedSeats(self):
        return self.seatsPlan.sum()

    def countFreeSeats(self):

        return self.length*self.width - self.seatsPlan.sum()


def main():

    length = 10
    width = 10
    sizeOfGroup = 2

    if(len(sys.argv)==2):
        length = int(sys.argv[1])
        width = length

    elif(len(sys.argv)==3):
        length = int(sys.argv[1])
        width = int(sys.argv[2])
    
    elif(len(sys.argv)==4):
        length = int(sys.argv[1])
        width = int(sys.argv[2])
        sizeOfGroup = int(sys.argv[3])


    #Statistical analysis
    GlobalSeatsOccupation = np.zeros((length, width)) #Used for visualization
    finalOccupations = []
    finalVacancies = []
    numOfSimulations = 1000

    #Main "Monte Carlo" Simulation...
    for sim in range(numOfSimulations): #FIXME: Pass it as argument
        print sim,', ',

        myTheater = Theater(length, width, sizeOfGroup=sizeOfGroup)
        
        for groupOfPeopleIdx in range(myTheater.length*myTheater.width): #Just a put a large number to have enough. Best case groupSize=1 so I can fit all

            if( len(myTheater.vacantSeats) == 0 and groupOfPeopleIdx != 0): #i!=0 is for the first time that the vacantSeats vector will be empty...
                GlobalSeatsOccupation = GlobalSeatsOccupation + myTheater.seatsPlan # That should be the final plan...

                finalOccupations.append(myTheater.countOccupiedSeats())
                finalVacancies.append(myTheater.countFreeSeats())
                
                break #We start a new simulation!!

            else:

                myTheater.findEmptyBlocksForGroup()
                myTheater.occupyRandomEmptyBlock()


    print '\n\nStatistical Analysis\n---------------------------------\n'

    print 'Theater size:\t', myTheater.length, ' x ', myTheater.width
    print 'Total seats:\t', myTheater.totalSeats
    print 'Number of simulations:\t', numOfSimulations
    print 'Size of group of people:\t', sizeOfGroup

    print '\n\n'
    print 'Mean number of Vacancies:\t', 0.0 if len(finalVacancies)==0 else np.average(finalVacancies)
    print 'Std of Vacancies:\t', 0.0 if len(finalVacancies)==0 else np.std(finalVacancies)
    print 'Probability:\t', 0.0 if len(finalVacancies)==0 else np.average(finalVacancies)/myTheater.totalSeats
    print 'Theoretical Probability for reference for couples(sizeOfGroup=2), i.e., 1/(e^2):\t', 1/math.exp(2)

    print '\n\n'
    print 'Mean number of Occupations:\t', 0.0 if len(finalOccupations)==0 else np.average(finalOccupations)
    print 'Std of Occupations:\t', 0.0 if len(finalOccupations)==0 else np.std(finalOccupations)
    print 'Probability:\t',  0.0 if len(finalOccupations)==0 else np.average(finalOccupations)/myTheater.totalSeats

    print '\n\n'
    
    pylab.imshow(GlobalSeatsOccupation)
    pylab.colorbar()
    pylab.show()


if( __name__ == "__main__"):
    main()

